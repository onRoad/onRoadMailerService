//Import the necessary libraries/declare the necessary objects
let express = require("express");
let myParser = require("body-parser");
let app = express();
let nodemailer = require('nodemailer');

app.use(myParser.urlencoded({
    extended: true
}));

app.use(myParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post("/email/send", function (request, response) {

    let mailOptions = {
        from: 'onroadmailer@gmail.com',
        to: request.body.to,
        subject: request.body.subject,
        html: request.body.body
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            response.sendStatus(500, error);
        } else {
            response.status(200);
            response.send({});
        }
    });
});

let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'onroadmailer@gmail.com',
        pass: 'OnRoad2018.'
    }
});

//Start the server and make it listen for connections on port 8080
app.listen(8087);